import httpx
from config.settings import settings
from .rate_limits import rate_limit_retry
from logs.logger import logger

@rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429])
async def check_dns_record(ioc_value):
    async with httpx.AsyncClient() as client:
        url = "https://api.geekflare.com/dnsrecord"
        
        headers = {
            "x-api-key": settings.GEEKFLARE_API_KEY,
            "Content-Type": "application/json",
        }
        data = {
            "url": ioc_value,
            "proxyCountry": "tr",
            "followRedirect": True,
        }

        response = await client.post(url, headers=headers, json=data)

        if response.status_code == 200:
            response_data = response.json()
            return response_data
        else:
            logger.error(f"Failed to fetch DNS records for {ioc_value}. Status code: {response.status_code}")
