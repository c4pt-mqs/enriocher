import httpx
from config.settings import settings
from .rate_limits import rate_limit_retry
from logs.logger import logger

censys_client = httpx.AsyncClient()

@rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429])
async def check_censys(ioc_value):
    url = f"https://search.censys.io/api/v2/certificates/search?q={ioc_value}"
    headers = {
        "accept": "application/json",
        "Authorization": f"Basic {settings.CENSYS_API_ID_SECRET}"
    }

    try:
        response = await censys_client.get(url, headers=headers)

        if response.status_code == 200:
            response_data = response.text
            return response_data
        else:
            logger.error(f"Failed to fetch Censys data for {ioc_value}. Status code: {response.status_code}")
            return None
    except httpx.RequestError as e:
        logger.error(f"Error while fetching data from Censys: {e}")
        return None