import httpx
from config.settings import settings
from .rate_limits import rate_limit_retry
from logs.logger import logger

@rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429])
async def check_geo_location(ioc_value):
    url = f"http://api.ipstack.com/{ioc_value}"
    params = {
        "access_key": settings.IPSTACK_API_KEY
    }
    
    async with httpx.AsyncClient() as client:
        response = await client.get(url, params=params)

        if response.status_code == 200:
            result = response.json()
            return f"{result['country_name']}, {result['region_name']}, {result['city']}"
        else:
            logger.error(f"Failed to fetch GEO location for {ioc_value}. Status code: {response.status_code}")
