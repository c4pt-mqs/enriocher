import httpx
import socket
from config.settings import settings
from .rate_limits import rate_limit_retry

@rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429])
async def get_ip_from_domain(domain):
    try:
        ip_address = socket.gethostbyname(domain)
        return ip_address
    except socket.gaierror:
        raise Exception(f"Invalid domain: {domain}")

@rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429])
async def check_abuseipdb(ioc_value):
    ip_address = await get_ip_from_domain(ioc_value)

    url = "https://api.abuseipdb.com/api/v2/check"
    params = {
        "ipAddress": ip_address,
        "maxAgeInDays": "90"
    }
    headers = {
        "Accept": "application/json",
        "Key": settings.ABUSEIPDB_API_KEY,
    }

    try:
        async with httpx.AsyncClient() as client:
            response = await client.get(url, headers=headers, params=params)

            if response.status_code == 200:
                data = response.json()
                return data
            else:
                raise Exception(f"Failed to fetch AbuseIPDB data for {ioc_value}. Status code: {response.status_code}")
    except httpx.RequestError as e:
        raise Exception(f"Error while fetching data from AbuseIPDB: {e}")
