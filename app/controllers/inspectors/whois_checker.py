import httpx
from config.settings import settings
from .rate_limits import rate_limit_retry
from logs.logger import logger

@rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429])
async def check_whois(ioc_value):
    async with httpx.AsyncClient() as client:
        url = f"https://www.whoisxmlapi.com/whoisserver/WhoisService?apiKey={settings.WHOISXML_API_KEY}&domainName={ioc_value}&outputFormat=json"
        response = await client.get(url)

        if response.status_code == 200:
            response_data = response.json()
            return response_data
        else:
            logger.error(f"Failed to fetch DNS records for {ioc_value}. Status code: {response.status_code}")
