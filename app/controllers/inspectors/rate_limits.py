import asyncio
import httpx

def rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429]):
    def decorator(func):
        async def wrapper(*args, **kwargs):
            retries = 0
            while retries < max_retries:
                try:
                    return await func(*args, **kwargs)
                except (httpx.ReadTimeout, httpx.HTTPStatusError) as exc:
                    if isinstance(exc, httpx.HTTPStatusError) and exc.response.status_code in retry_status_codes:
                        retries += 1
                        await asyncio.sleep(delay * (backoff ** retries))
                    else:
                        raise
            raise Exception(f"Max retries exceeded for {func.__name__}")
        return wrapper
    return decorator