import httpx
from .rate_limits import rate_limit_retry

@rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429])
async def check_urlscan(ioc_value):
    async with httpx.AsyncClient() as client:
        url = f"https://urlscan.io/api/v1/search/?q={ioc_value}"
        headers = {
            "Content-Type": "application/json",
        }
        try:
            response = await client.get(url, headers=headers)
            response.raise_for_status()
            data = response.json()
            return data
        except httpx.HTTPStatusError as e:
            raise Exception(f"Failed to fetch urlscan.io data for {ioc_value}. Status code: {e.response.status_code}")
        except httpx.RequestError as e:
            raise Exception(f"Request error while fetching urlscan.io data for {ioc_value}: {str(e)}")
        except httpx.JSONDecodeError:
            raise Exception(f"Failed to decode JSON response for {ioc_value}.")
