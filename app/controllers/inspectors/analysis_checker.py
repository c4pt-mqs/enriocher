import httpx
from config.settings import settings
from .rate_limits import rate_limit_retry
from logs.logger import logger

@rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429])
async def check_analysis(ioc_value):
    url = f"https://www.virustotal.com/api/v3/domains/{ioc_value}"
    headers = {"x-apikey": settings.VIRUSTOTAL_API_KEY}

    async with httpx.AsyncClient() as virustotal_client:
        try:
            response = await virustotal_client.get(url, headers=headers)
            response.raise_for_status()

            result = response.json()
            analysis_results = result["data"]["attributes"]["last_analysis_results"]
            harmless_count = 0
            harmful_count = 0

            for result in analysis_results.values():
                category = result["category"]
                if category == "harmless":
                    harmless_count += 1
                elif category == "harmful":
                    harmful_count += 1

            result_data = {
                "harmless_count": harmless_count,
                "harmful_count": harmful_count
            }
            return result_data

        except httpx.HTTPStatusError as e:
            logger.error(f"Failed to fetch VirusTotal analysis data for {ioc_value}. Status code: {e.response.status_code}")
            raise Exception(f"Failed to fetch VirusTotal analysis data for {ioc_value}. Status code: {e.response.status_code}")
        except httpx.RequestError as e:
            logger.error(f"Request error while fetching VirusTotal analysis data for {ioc_value}: {str(e)}")
            raise Exception(f"Request error while fetching VirusTotal analysis data for {ioc_value}: {str(e)}")
        except httpx.JSONDecodeError:
            logger.error(f"Failed to decode JSON response for {ioc_value}.")
            raise Exception(f"Failed to decode JSON response for {ioc_value}.")
