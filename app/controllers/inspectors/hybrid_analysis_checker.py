import httpx
from .rate_limits import rate_limit_retry
from logs.logger import logger
from config.settings import settings

@rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429])
async def check_hybrid_analysis(ioc_value: str):
    url = "https://www.hybrid-analysis.com/api/v2/quick-scan/url"
    headers = {
        "accept": "application/json",
        "user-agent": "Falcon Sandbox",
        "api-key": settings.HYBRID_ANALYSIS_API_KEY,
        "content-type": "application/x-www-form-urlencoded",
    }
    data = {
        "url": ioc_value,
        "scan_type": "all"
    }

    async with httpx.AsyncClient() as client:
        try:
            response = await client.post(url, headers=headers, data=data)
            response.raise_for_status()

            analysis_url = response.json()
            return analysis_url

        except httpx.HTTPStatusError as e:
            logger.error(f"Failed to fetch analysis hybrid for {ioc_value}. Status code: {e.response.status_code}")
            raise Exception(f"Failed to fetch analysis hybrid for {ioc_value}. Status code: {e.response.status_code}")
        except httpx.RequestError as e:
            logger.error(f"Request error while fetching analysis hybrid for {ioc_value}: {str(e)}")
            raise Exception(f"Request error while fetching analysis hybrid for {ioc_value}: {str(e)}")
        except httpx.JSONDecodeError:
            logger.error(f"Failed to decode JSON response for {ioc_value}.")
            raise Exception(f"Failed to decode JSON response for {ioc_value}.")
