import httpx
from .rate_limits import rate_limit_retry
from logs.logger import logger

@rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429])
async def check_ssl_certificate(ioc_value):
    async with httpx.AsyncClient() as client:
        url = f"https://crt.sh/?q={ioc_value}&output=json"
        response = await client.get(url)

        if response.status_code == 200:
            try:
                response_data = response.json()
                return response_data
            except httpx.JSONDecodeError:
                logger.error(f"Failed to decode JSON response for {ioc_value}")
                return None
        else:
            logger.error(f"Failed to fetch SSL certificate information for {ioc_value}. Status code: {response.status_code}")
            return None
