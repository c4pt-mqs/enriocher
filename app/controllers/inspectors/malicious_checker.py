import httpx
from config.settings import settings
from .rate_limits import rate_limit_retry
from logs.logger import logger

@rate_limit_retry(max_retries=3, delay=1, backoff=2, retry_status_codes=[429])
async def check_malicious(ioc_value):
    async with httpx.AsyncClient() as client:
        url = f'https://otx.alienvault.com/api/v1/indicators/domain/{ioc_value}'
        headers = {'X-OTX-API-KEY': settings.OTX_API_KEY}
        response = await client.get(url, headers=headers)

        if response.status_code == 200:
            try:
                data = response.json()
                return data
            except httpx.JSONDecodeError:
                logger.error(f"Failed to decode JSON response for {ioc_value}")
                return None
        else:
            logger.error(f"Failed to fetch malicious information for {ioc_value}. Status code: {response.status_code}")
            return None
