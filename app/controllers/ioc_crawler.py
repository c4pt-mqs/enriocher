import asyncio
from models.schemas import IoCType
from datetime import datetime
from logs.logger import logger
from aiocache import Cache
from models.ioc_model import IoCModel, AsyncSessionLocal, create_tables
from .inspectors.analysis_checker import check_analysis
from .inspectors.dns_record_checker import check_dns_record
from .inspectors.geo_location_checker import check_geo_location
from .inspectors.malicious_checker import check_malicious
from .inspectors.ssl_certificate_checker import check_ssl_certificate
from .inspectors.whois_checker import check_whois
from .inspectors.censys_checker import check_censys
from .inspectors.urlscan_checker import check_urlscan
from .inspectors.abuseipdb_checker import check_abuseipdb
from .inspectors.pulsedive_checker import check_pulsedive
from .inspectors.hybrid_analysis_checker import check_hybrid_analysis

cache = Cache(Cache.MEMORY)

async def analyze_ioc(ioc_value):
    cached_result = await cache.get(ioc_value)
    if cached_result:
        return cached_result

    await create_tables()

    results = await asyncio.gather(
        check_malicious(ioc_value),
        check_geo_location(ioc_value),
        check_analysis(ioc_value),
        check_whois(ioc_value),
        check_ssl_certificate(ioc_value),
        check_dns_record(ioc_value),
        check_censys(ioc_value),
        check_urlscan(ioc_value),
        check_abuseipdb(ioc_value),
        check_pulsedive(ioc_value),
        check_hybrid_analysis(ioc_value),
    )

    results = dict(zip(IoCType, results))

    await cache.set(ioc_value, results)

    logger.info(f"Starting analysis for IoC value: {ioc_value}")

    async with AsyncSessionLocal() as db:
        try:
            ioc_model_object = IoCModel(
                added_at=datetime.now(),
                geometric_location=str(results[IoCType.GEO_LOCATION]),
                malicious_control=str(results[IoCType.MALICIOUS_CONTROL]),
                analysis=str(results[IoCType.ANALYSIS]),
                whois=str(results[IoCType.WHOIS]),
                ssl_certificate=str(results[IoCType.SSL_CERTIFICATE]),
                dns_record=str(results[IoCType.DNS_RECORD]),
                censys=str(results[IoCType.CENSYS]),
                urlscan=str(results[IoCType.URLSCAN]),
                abuseipdb=str(results[IoCType.ABUSEIPDB]),
                pulsedive=str(results[IoCType.PULSEDIVE]),
                hybrid_analysis=str(results[IoCType.HYBRID_ANALYSIS]),
            )
            db.add(ioc_model_object)
            await db.commit()
        finally:
            await db.close()

    return results
