from sqlalchemy import MetaData, Table, Column, Integer, JSON, DateTime
from config.settings import settings
from sqlalchemy.ext.asyncio import create_async_engine, AsyncSession
from sqlalchemy.orm import declarative_base, sessionmaker

Base = declarative_base()
meta = MetaData()
engine = create_async_engine(settings.DATABASE_URL)
AsyncSessionLocal = sessionmaker(bind=engine, class_=AsyncSession, autocommit=False, autoflush=False)

async def get_db():
    return AsyncSessionLocal()

class IoCModel(Base):
    ioc_analysis_table = Table(
        'ioc_analysis',
        meta,
        Column('id', Integer, primary_key=True),
        Column('added_at', DateTime),
        Column('geometric_location', JSON),
        Column('malicious_control', JSON),
        Column('analysis', JSON),
        Column('whois', JSON),
        Column('ssl_certificate', JSON),
        Column('dns_record', JSON),
        Column('censys', JSON),
        Column('urlscan', JSON),
        Column('abuseipdb', JSON),
        Column('pulsedive', JSON),
        Column('hybrid_analysis', JSON),
    )
    
    __table__ = ioc_analysis_table

async def create_tables():
    async with engine.begin() as conn:
        await conn.run_sync(meta.create_all)
