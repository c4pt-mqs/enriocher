from enum import Enum
from pydantic import BaseModel, ValidationError

class IoCType(str, Enum):
    MALICIOUS_CONTROL = "malicious_control"
    GEO_LOCATION = "geometric_location"
    ANALYSIS = "analysis"
    WHOIS = "whois"
    SSL_CERTIFICATE = "ssl_certificate"
    DNS_RECORD = "dns_record"
    CENSYS = "censys"
    URLSCAN = "urlscan"
    ABUSEIPDB = "abuseipdb"
    PULSEDIVE = "pulsedive"
    HYBRID_ANALYSIS = "hybrid_analysis"
    
class IoCValue(BaseModel):
    ioc_value: str
    
try:
    IoCValue()
except ValidationError as exc:
    print(repr(exc.errors()[0]['type']))
    #> 'missing'