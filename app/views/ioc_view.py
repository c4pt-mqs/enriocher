import re
from fastapi import FastAPI, Query, Request, HTTPException
from controllers.ioc_crawler import analyze_ioc
import time

app = FastAPI()

# root domain: 1-63 | TLD: 2-63 | Total: 253
IOC_VALUE_REGEX = r"^(?!https?://)(?!-)(?![0-9]*\.)[a-z0-9\-\.]{1,63}(?<!-)(?<!\.)\.[a-z]{2,63}$"

@app.middleware("http")
async def add_process_time_header(request: Request, call_next):
    start_time = time.time()
    response = await call_next(request)
    process_time = time.time() - start_time
    response.headers["X-Process-Time"] = str(process_time)
    return response

@app.get("/")
async def root_dir():
    return {"message": "Server is running!"}

@app.get("/search")
async def search_ioc(ioc_value: str = Query(..., alias="ioc_value")):
    if not re.match(IOC_VALUE_REGEX, ioc_value):
        raise HTTPException(status_code=400, detail="Invalid IoC value. Please provide a valid value.")

    converted_ioc_value = ioc_value.strip()
    results = await analyze_ioc(converted_ioc_value)
    return results
