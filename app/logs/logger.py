import logging
from logging.handlers import RotatingFileHandler

def setup_logger():
    logger = logging.getLogger("ioc_enrichment")
    logger.setLevel(logging.DEBUG)

    log_file = "./logs/app.log"
    file_handler = RotatingFileHandler(log_file, maxBytes=100000, backupCount=5)
    file_handler.setLevel(logging.DEBUG)
    console_handler = logging.StreamHandler()
    console_handler.setLevel(logging.INFO)

    formatter = logging.Formatter(
        "%(asctime)s - %(name)s - %(levelname)s - %(message)s"
    )
    file_handler.setFormatter(formatter)
    console_handler.setFormatter(formatter)
    logger.addHandler(file_handler)
    logger.addHandler(console_handler)

    return logger

logger = setup_logger()
