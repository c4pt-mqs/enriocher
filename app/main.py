import uvicorn
from logs.logger import logger
from views.ioc_view import app
from config.settings import settings

if __name__ == "__main__":
    logger.info("Starting the IoC Enrichment API server...")
    uvicorn.run(app, host=settings.API_HOST, port=settings.API_PORT)

## python3 main.py
## uvicorn main:app --reload
## docker-compose up --build -d