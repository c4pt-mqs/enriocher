# EnrIoCher

The IoC Enrichment API is a Python application that provides enrichment and analysis for Indicators of Compromise (IoCs) using various threat intelligence services. It allows users to query and analyze different IoC types and receive relevant information about their potential malicious activities.

## Features

- Enrichment of various IoC types, with domain names.
- Integration with multiple threat intelligence services, such as VirusTotal, AbuseIPDB, Censys, URLScan, and more.
- Caching mechanism to improve response times for frequently queried IoCs.
- Data storage and retrieval using a PostgreSQL database for historical analysis.
- Easy-to-use API endpoints for querying and retrieving IoC analysis results.

## Getting Started

Follow these steps to set up the IoC Enrichment API on your local machine:

1. Install Docker and Docker Compose on your system.
2. Clone this repository to your local machine using the following command:

```
git clone https://github.com/c4pt-mqs/enriocher.git
```

3. Navigate to the project directory:

```
cd enriocher/
```

4. If you want to execute in your local machine using any of the following commands:

```
python3 main.py
uvicorn main:app --reload
```

5. Build and run the Docker containers:

```
docker-compose up --build -d
```

6. Once the containers are up and running, you can access the API at `http://localhost:8000`.

## API Endpoints

- `GET /`: Returns a simple message to verify that the server is running.
- `GET /search?ioc_value=<IOC_VALUE>`: Performs analysis for the provided IoC value. Replace `<IOC_VALUE>` with the domain you want to analyze.

## Configuration

You can configure the API settings by modifying the `settings.toml` file in the `app/config` directory. This file contains API host, port, and API keys for various threat intelligence services.
